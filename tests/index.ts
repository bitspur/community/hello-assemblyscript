/**
 * File: /package.json
 * Project: hello-assemblyscript
 * File Created: 31-08-2021 22:57:25
 * Author: Clay Risser <clayrisser@gmail.com>
 * -----
 * Last Modified: 31-08-2021 23:54:40
 * Modified By: Clay Risser <clayrisser@gmail.com>
 * -----
 * Clay Risser (c) Copyright 2021
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('hello-assemblyscript', () => {
  it('should work', async () => {
    expect(true).toBe(true);
  });
});

export default null;
